<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'gasStationID', 'quantity'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id', 'gasStationID', 'updated_at'];

    /**
     * Returns the customer of the given order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'username');
    }

    /**
     * Returns the gas station for the given order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gasstation()
    {
        return $this->belongsTo('App\Gasstation', 'gasStationID');
    }
}
