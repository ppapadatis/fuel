<?php

namespace App\Api\V1\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class PutPriceDataRequest extends FormRequest
{
    /**
     * @return mixed
     */
    public function rules()
    {
        return Config::get('boilerplate.gas_station.put_price_data.validation_rules');
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
