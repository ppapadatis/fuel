<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Requests\PostOrderRequest;
use App\Api\V1\Requests\PutPriceDataRequest;
use App\Order;
use JWTAuth;
use App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;

class GasStationController extends Controller
{
    use Helpers;

    /**
     * Current user.
     *
     * @var \App\User
     */
    private $currentUser;

    /**
     * GasStationController constructor.
     */
    public function __construct()
    {
        // Check for API token.
        $this->middleware(function($request, $next) {
            if (!$currentUser = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['error' => 'user_not_found'], 404);
            }

            $this->currentUser = $currentUser;
            return $next($request);
        });
    }

    /**
     * GET: Returns all orders for the station owner.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrders()
    {
        try {
            if (!$this->currentUser->is_owner) {
                return response()->json(['error' => 'invalid_user_type'], 400);
            }

            $gasStation = $this->currentUser->gasstation()->first();
            if (!$gasStation) return response()->json();

            $orders = $gasStation->orders()->get();
            return response()->json($orders);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * GET: Returns the price data of a gas station for the station owner.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPriceData()
    {
        try {
            if (!$this->currentUser->is_owner) {
                return response()->json(['error' => 'invalid_user_type'], 400);
            }

            $gasStation = $this->currentUser->gasstation()->first();
            if (!$gasStation) return response()->json();
            $prices = $gasStation->pricedata()->get();
            return response()->json($prices);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * PUT: Updates price data of a gas station for the station owner.
     *
     * @param \App\Api\V1\Requests\PutPriceDataRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function putPriceData(PutPriceDataRequest $request)
    {
        try {
            if (!$this->currentUser->is_owner) {
                return response()->json(['error' => 'invalid_user_type'], 400);
            }

            $gasStation = $this->currentUser->gasstation()->first();
            if (!$gasStation) return response()->json();
            $priceData = $gasStation->pricedata()->get();

            $priceDataToChange = $priceData->where('fuelTypeID', '=', $request->get('fuelTypeID'))
                ->where('fuelSubTypeID', '=', $request->get('fuelSubTypeID'))->first();

            if (!$priceDataToChange) return response()->json(['error' => 'price_data_not_found'], 404);

            $priceDataToChange->update([
                'fuelName' => $request->get('fuelName'),
                'fuelPrice' => $request->get('fuelPrice'),
                'isPremium' => $request->get('isPremium')
            ]);

            return response()->json($priceDataToChange);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * POST: Creates an order for the customer.
     *
     * @param \App\Api\V1\Requests\PostOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postOrder(PostOrderRequest $request)
    {
        try {
            if ($this->currentUser->is_owner) {
                return response()->json(['error' => 'invalid_user_type'], 400);
            }

            $order = Order::create([
                'username' => $this->currentUser->username,
                'gasStationID' => $request->get('gasStationID'),
                'quantity' => $request->get('quantity')
            ]);
            if (!$order->save()) throw new \PDOException();

            return response()->json($order);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }
}
