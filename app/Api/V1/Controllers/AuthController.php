<?php

namespace App\Api\V1\Controllers;

use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use App\Api\V1\Requests\RegisterRequest;

class AuthController extends Controller
{
    /**
     * @param \App\Api\V1\Requests\LoginRequest $request
     * @param \Tymon\JWTAuth\JWTAuth            $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        try {
            $credentials = $request->only(['username', 'password']);
            $token = $JWTAuth->attempt($credentials);
            if (!$token) return response()->json(['error' => 'access_denied'], 401);

            return response()->json([
                'status' => 'OK',
                'token' => $token,
                'user_type' => $JWTAuth->toUser($token)->is_owner
            ]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(JWTAuth $JWTAuth)
    {
        try {
            $JWTAuth->invalidate($JWTAuth->getToken());
            return response()->json(['status' => 'OK']);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * @param \App\Api\V1\Requests\RegisterRequest $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request, JWTAuth $JWTAuth)
    {
        try {
            $user = new User($request->all());
            if (!$user->save()) throw new \PDOException();
            $token = $JWTAuth->fromUser($user);
            return response()->json([
                'status' => 'OK',
                'token' => $token,
                'user_type' => $user->is_owner
            ], 201);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }
}
