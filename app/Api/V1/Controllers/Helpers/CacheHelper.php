<?php

namespace App\Api\V1\Controllers\Helpers;

use App\Events\GasStationsEvent;
use App\Events\PriceDataEvent;
use App\Events\UserEvent;
use Cache;

class CacheHelper
{
    /**
     * Gets all users from the cache.
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getCachedUsers()
    {
        if (!Cache::has(ConstantsHelper::DB_USERS)) {
            event(new UserEvent());
        }

        return Cache::get(ConstantsHelper::DB_USERS);
    }

    /**
     * Gets all gas stations from the cache.
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getCachedGasStations()
    {
        if (!Cache::has(ConstantsHelper::DB_GAS_STATIONS)) {
            event(new GasStationsEvent());
        }

        return Cache::get(ConstantsHelper::DB_GAS_STATIONS);
    }

    /**
     * Gets all price data from the cache.
     *
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public static function getCachedPriceData()
    {
        if (!Cache::has(ConstantsHelper::DB_PRICE_DATA)) {
            event(new PriceDataEvent());
        }

        return Cache::get(ConstantsHelper::DB_PRICE_DATA);
    }

    /**
     * Removes from the cache the given identifier.
     *
     * @param $identifier
     */
    public static function clearCache($identifier)
    {
        Cache::forget($identifier);
    }

    /**
     * Removes all data from cache.
     *
     * @return void
     */
    public static function flushCache()
    {
        Cache::forget(ConstantsHelper::DB_USERS);
        Cache::forget(ConstantsHelper::DB_GAS_STATIONS);
        Cache::forget(ConstantsHelper::DB_PRICE_DATA);
    }
}
