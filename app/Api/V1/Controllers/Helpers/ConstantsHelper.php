<?php

namespace App\Api\V1\Controllers\Helpers;

class ConstantsHelper
{
    /**
     * Identifier for cached users.
     *
     * @const DB_USERS
     */
    CONST DB_USERS = 'dbUsers';

    /**
     * Identified for cached gas stations.
     *
     * @const DB_GAS_STATIONS
     */
    CONST DB_GAS_STATIONS = 'dbGasStations';

    /**
     * Identified for cached price data.
     *
     * @const DB_PRICE_DATA
     */
    CONST DB_PRICE_DATA = 'dbPriceData';
}
