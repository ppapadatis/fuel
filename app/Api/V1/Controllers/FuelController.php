<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Controllers\Helpers\CacheHelper;
use App\Http\Controllers\Controller;

class FuelController extends Controller
{
    /**
     * GET: Returns the total number of all gas stations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountOfGasStations()
    {
        try {
            $gasStations = CacheHelper::getCachedGasStations();
            return response()->json($gasStations->count());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * GET: Returns the max, min and average price per lt^4.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPriceDataPerLitre()
    {
        try {
            $pricedata = CacheHelper::getCachedPriceData();

            $data = [
                'max' => round($pricedata->max('fuelPrice'), 3),
                'min' => round($pricedata->min('fuelPrice'), 3),
                'average' => round($pricedata->average('fuelPrice'), 3)
            ];

            return response()->json($data);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * GET: Returns pricedata in row chunks for chart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChartPriceData()
    {
        try {
            $gasStations = CacheHelper::getCachedGasStations();

            $data = [];
            $data[] = ['ΠΡΑΤΗΡΙΑ', 'ΠΟΣΟΤΗΤΑ'];
            foreach ($gasStations as $gasStation) {
                $data[] = [$gasStation->gasStationOwner, $gasStation->pricedata()->count()];
            }

            return response()->json($data);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * GET: Returns the price data for the requested gas station.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPriceDataByGasStation($id)
    {
        try {
            if (is_null($id) || !filter_var($id, FILTER_VALIDATE_INT) || $id < 1) {
                return response()->json(['error' => 'invalid_id'], 400);
            }

            $pricedata = CacheHelper::getCachedPriceData()->where('gasStationID', '=', $id);
            if (!$pricedata) return response()->json(['error' => 'gas_station_not_found'], 404);

            return response()->json($pricedata);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * GET: Returns all gas stations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllGasStations()
    {
        try {
            $gasStations = CacheHelper::getCachedGasStations();
            return response()->json($gasStations);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }

    /**
     * GET: Returns all gas stations in XML format.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllGasStationsAsXml()
    {
        try {
            $gasStations = CacheHelper::getCachedGasStations();
            return response()->xml($gasStations->toArray());
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
    }
}
