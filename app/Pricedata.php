<?php

namespace App;

use Carbon\Carbon;
use App\Events\PriceDataEvent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Pricedata extends Model
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pricedata';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = ['gasStationID', 'fuelTypeID', 'fuelSubTypeID'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['dateUpdated'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gasStationID', 'fuelTypeID', 'fuelSubTypeID',
        'fuelNormalName', 'fuelName', 'fuelPrice', 'isPremium'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['gasStationID', 'fuelNormalName'];

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        if (is_array($this->primaryKey)) {
            foreach ($this->primaryKey as $pk) {
                $query->where($pk, '=', $this->original[$pk]);
            }
            return $query;
        } else {
            return parent::setKeysForSaveQuery($query);
        }
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::created(function($model) {
            event(new PriceDataEvent($model));
        });

        static::updating(function($model) {
            $model->dateUpdated = Carbon::now();
        });

        static::updated(function($model) {
            event(new PriceDataEvent($model));
        });

        static::deleted(function($model) {
            event(new PriceDataEvent($model));
        });
    }

    /**
     * Returns the gas station of the given price data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gasstations()
    {
        return $this->belongsTo('App\Gasstation', 'gasStationID');
    }
}
