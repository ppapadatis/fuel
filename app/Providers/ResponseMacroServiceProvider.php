<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('xml', function(array $vars, $status = 200, array $header = [], $xml = null)
        {
            if (is_null($xml)) {
                $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><response/>');
            }

            foreach ($vars as $key => $value) {
                if (is_array($value)) {
                    if (is_numeric($key)) {
                        Response::xml($value, $status, $header, $xml->addChild(str_singular($xml->getName())));
                    } else {
                        Response::xml($value, $status, $header, $xml->addChild($key));
                    }
                } else {
                    $xml->addChild($key, htmlspecialchars($value));
                }
            }

            if (empty($header)) {
                $header['Content-Type'] = 'application/xml;charset=utf-8';
            }

            return Response::make($xml->asXML(), $status, $header);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
