<?php

namespace App\Events;

use App\Gasstation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GasStationsEvent
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var Gasstation
     */
    public $gasStation;

    /**
     * Create a new event instance.
     *
     * @param Gasstation $gasStation
     *
     * @return void
     */
    public function __construct(Gasstation $gasStation = null)
    {
        $this->gasStation = $gasStation;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('gasstations-channel');
    }
}
