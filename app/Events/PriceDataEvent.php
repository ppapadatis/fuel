<?php

namespace App\Events;

use App\Pricedata;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PriceDataEvent
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var Pricedata
     */
    public $priceData;

    /**
     * Create a new event instance.
     *
     * @param Pricedata $priceData
     *
     * @return void
     */
    public function __construct(Pricedata $priceData = null)
    {
        $this->priceData = $priceData;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('pricedata-channel');
    }
}
