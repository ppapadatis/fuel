<?php

namespace App\Listeners;

use App\Api\V1\Controllers\Helpers\ConstantsHelper;
use Cache;
use App\Events\PriceDataEvent;
use App\Pricedata;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PriceDataEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PriceDataEvent  $event
     * @return void
     */
    public function handle(PriceDataEvent $event)
    {
        Cache::put(ConstantsHelper::DB_PRICE_DATA, Pricedata::all(), 3600);
    }
}
