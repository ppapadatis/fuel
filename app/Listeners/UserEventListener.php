<?php

namespace App\Listeners;

use App\Api\V1\Controllers\Helpers\ConstantsHelper;
use App\User;
use Cache;
use App\Events\UserEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserEvent  $event
     * @return void
     */
    public function handle(UserEvent $event)
    {
        Cache::put(ConstantsHelper::DB_USERS, User::all(), 3600);
    }
}
