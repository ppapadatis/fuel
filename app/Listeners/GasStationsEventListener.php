<?php

namespace App\Listeners;

use App\Api\V1\Controllers\Helpers\ConstantsHelper;
use Cache;
use App\Events\GasStationsEvent;
use App\Gasstation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GasStationsEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GasStationsEvent  $event
     * @return void
     */
    public function handle(GasStationsEvent $event)
    {
        Cache::put(ConstantsHelper::DB_GAS_STATIONS, Gasstation::with('pricedata')->get(), 3600);
    }
}
