<?php

namespace App;

use App\Events\GasStationsEvent;
use Illuminate\Database\Eloquent\Model;

class Gasstation extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'gasStationID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gasStationLat', 'gasStationLong', 'fuelCompID', 'fuelCompNormalName', 'gasStationOwner',
        'ddID', 'ddNormalName', 'municipalityID', 'municipalityNormalName', 'countyID',
        'countyName', 'gasStationAddress', 'phone1', 'username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'fuelCompID', 'ddID', 'municipalityID', 'countyID', 'username'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        static::created(function($model) {
            event(new GasStationsEvent($model));
        });

        static::updated(function($model) {
            event(new GasStationsEvent($model));
        });

        static::deleted(function($model) {
            event(new GasStationsEvent($model));
        });
    }

    /**
     * Returns the owner of the gas station.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'username');
    }

    /**
     * Returns the prices of the gas station.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pricedata()
    {
        return $this->hasMany('App\Pricedata', 'gasStationID', 'gasStationID');
    }

    /**
     * Returns the orders placed of the gas station.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order', 'gasStationID', 'gasStationID');
    }
}
