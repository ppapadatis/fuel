<div class="modal fade" id="priceModal" tabindex="-1" role="dialog" aria-labelledby="priceModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΠΑΡΑΓΓΕΛΙΑ</h4>
            </div>
            <form autocomplete="off" role="form" data-toggle="validator" novalidate>
                <div class="modal-body">
                    <input type="hidden" name="fuelTypeID" id="fuelTypeID" value="">
                    <input type="hidden" name="fuelSubTypeID" id="fuelSubTypeID" value="">

                    <div class="form-group">
                        <label for="fuelName">ΟΝΟΜΑ ΚΑΥΣΙΜΟΥ</label>
                        <input type="text" class="form-control" name="fuelName" id="fuelName" placeholder="ΟΝΟΜΑ ΚΑΥΣΙΜΟΥ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="fuelPrice">ΤΙΜΗ ΚΑΥΣΙΜΟΥ (ΕΥΡΩ)</label>
                        <input type="text" class="form-control" name="fuelPrice" id="fuelPrice" placeholder="ΤΙΜΗ ΚΑΥΣΙΜΟΥ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="isPremium" id="isPremium"> Premium ΠΟΙΟΤΗΤΑ
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#priceListModal" data-dismiss="modal" data-backdrop="static">ΑΚΥΡΩΣΗ</button>
                    <button type="submit" class="btn btn-primary price_update" disabled>ΕΝΗΜΕΡΩΣΗ</button>
                </div>
            </form>
        </div>
    </div>
</div>
