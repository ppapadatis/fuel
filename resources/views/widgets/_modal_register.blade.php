<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΕΓΓΡΑΦΗ</h4>
            </div>
            <form autocomplete="off" role="form" data-toggle="validator" novalidate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username">ΟΝΟΜΑ ΧΡΗΣΤΗ</label>
                        <input type="text" class="form-control" name="username" id="username" placeholder="ΟΝΟΜΑ ΧΡΗΣΤΗ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="password">ΣΥΝΘΗΜΑΤΙΚΟ</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="ΣΥΝΘΗΜΑΤΙΚΟ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">ΕΠΑΝΑΛΗΨΗ ΣΥΝΘΗΜΑΤΙΚΟΥ</label>
                        <input type="password" class="form-control" data-match="#password" data-match-error="Passwords do not match." name="password_confirmation" id="password_confirmation" placeholder="ΕΠΑΝΑΛΗΨΗ ΣΥΝΘΗΜΑΤΙΚΟΥ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label><input type="checkbox" id="is_owner" name="is_owner"> ΕΙΜΑΙ ΙΔΙΟΚΤΗΤΗΣ ΠΡΑΤΗΡΙΟΥ</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ΑΚΥΡΩΣΗ</button>
                    <button type="submit" class="btn btn-primary register" disabled>ΕΓΓΡΑΦΗ</button>
                </div>
            </form>
        </div>
    </div>
</div>
