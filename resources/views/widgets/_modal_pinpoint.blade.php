<div class="modal fade" id="pinPointModal" tabindex="-1" role="dialog" aria-labelledby="pinPointModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive hidden">
                    <table class="table table-striped table-hover">
                        <thead>
                            <th>ΚΑΥΣΙΜΟ</th>
                            <th>ΤΙΜΗ (€)</th>
                            <th>PREMIUM</th>
                            <th>ΤΕΛΕΥΤΑΙΑ ΕΝΗΜΕΡΩΣΗ</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="alert alert-info hidden" role="alert">
                    <i class="material-icons">warning</i>&nbsp;<span>ΔΕ ΒΡΕΘΗΚΑΝ ΑΠΟΤΕΛΕΣΜΑΤΑ</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ΚΛΕΙΣΙΜΟ</button>
            </div>
        </div>
    </div>
</div>
