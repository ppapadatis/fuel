<div class="modal fade" id="chartModal" tabindex="-1" role="dialog" aria-labelledby="chartModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΣΤΑΤΙΣΤΙΚΑ ΠΡΑΤΗΡΙΩΝ</h4>
            </div>
            <div class="modal-body">
                <div id="chart_div"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ΚΛΕΙΣΙΜΟ</button>
            </div>
        </div>
    </div>
</div>
