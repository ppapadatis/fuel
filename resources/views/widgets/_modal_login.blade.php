<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΣΥΝΔΕΣΗ</h4>
            </div>
            <form autocomplete="off" role="form" data-toggle="validator" novalidate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="login_username">ΟΝΟΜΑ ΧΡΗΣΤΗ</label>
                        <input type="text" class="form-control" name="login_username" id="login_username" placeholder="ΟΝΟΜΑ ΧΡΗΣΤΗ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="login_password">ΣΥΝΘΗΜΑΤΙΚΟ</label>
                        <input type="password" class="form-control" name="login_password" id="login_password" placeholder="ΣΥΝΘΗΜΑΤΙΚΟ" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ΑΚΥΡΩΣΗ</button>
                    <button type="submit" class="btn btn-primary login" disabled>ΣΥΝΔΕΣΗ</button>
                </div>
            </form>
        </div>
    </div>
</div>
