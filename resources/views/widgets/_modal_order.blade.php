<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΠΑΡΑΓΓΕΛΙΑ</h4>
            </div>
            <form autocomplete="off" role="form" data-toggle="validator" novalidate>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="gasStationID">ΚΑΤΑΣΤΗΜΑ</label>
                        <select class="form-control" id="gasStationID" name="gasStationID" required></select>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <label for="quantity">ΠΟΣΟΤΗΤΑ (ΛΙΤΡΑ)</label>
                        <input type="number" class="form-control" name="quantity" id="quantity" placeholder="ΠΟΣΟΤΗΤΑ (ΛΙΤΡΑ)" required>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ΑΚΥΡΩΣΗ</button>
                    <button type="submit" class="btn btn-primary order" disabled>ΥΠΟΒΟΛΗ</button>
                </div>
            </form>
        </div>
    </div>
</div>
