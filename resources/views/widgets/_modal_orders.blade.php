<div class="modal fade" id="ordersModal" tabindex="-1" role="dialog" aria-labelledby="ordersModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ΠΑΡΑΓΓΕΛΙΕΣ</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive hidden">
                    <table class="table table-striped table-hover">
                        <thead>
                            <th>ΠΕΛΑΤΗΣ</th>
                            <th>ΠΟΣΟΤΗΤΑ (ΛΙΤΡΑ)</th>
                            <th>ΗΜΕΡΟΜΗΝΙΑ</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
                <div class="alert alert-info hidden" role="alert">
                    <i class="material-icons">warning</i>&nbsp;<span>ΔΕ ΒΡΕΘΗΚΑΝ ΑΠΟΤΕΛΕΣΜΑΤΑ</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ΚΛΕΙΣΙΜΟ</button>
            </div>
        </div>
    </div>
</div>
