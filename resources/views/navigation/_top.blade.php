<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name') }}</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="auth"><a href="#" class="logout">ΑΠΟΣΥΝΔΕΣΗ</a></li>
                <li><a href="#" data-toggle="modal" data-target="#loginModal" data-backdrop="static">ΣΥΝΔΕΣΗ</a></li>
                <li><a href="#" data-toggle="modal" data-target="#registerModal" data-backdrop="static">ΕΓΓΡΑΦΗ</a></li>
            </ul>
        </div>
    </div>
</nav>
