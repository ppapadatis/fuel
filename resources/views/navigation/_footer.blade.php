<footer class="footer">
    <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
        <div class="container navbar-default">
            <div class="row">
                <div class="col-m-8">
                    <div class="pull-left">
                        <p class="text-muted">
                            <span id="total_stations"><i class="material-icons">local_gas_station</i><!-- placeholder --></span>
                            <span id="min_price"><i class="material-icons">arrow_downward</i><!-- placeholder --></span>
                            <span id="average_price"><i class="material-icons">remove</i><!-- placeholder --></span>
                            <span id="max_price"><i class="material-icons">arrow_upward</i><!-- placeholder --></span>
                        </p>
                    </div>
                </div>
                <div class="col-m-4">
                    <div class="pull-right">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#footbar" aria-expanded="false" aria-controls="footbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div id="footbar" class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="public"><a href="#" data-toggle="modal" data-target="#listModal">ΛΙΣΤΑ ΠΡΑΤΗΡΙΩΝ</a></li>
                                <li class="public"><a href="#" data-toggle="modal" data-target="#chartModal">ΣΤΑΤΙΣΤΙΚΑ ΠΡΑΤΗΡΙΩΝ</a></li>
                                <li class="auth owner"><a href="#" data-toggle="modal" data-target="#ordersModal" data-backdrop="static">ΠΑΡΑΓΓΕΛΙΕΣ</a></li>
                                <li class="auth owner"><a href="#" data-toggle="modal" data-target="#priceListModal" data-backdrop="static">ΕΝΗΜΕΡΩΣΗ ΤΙΜΩΝ</a></li>
                                <li class="auth user"><a href="#" data-toggle="modal" data-target="#orderModal" data-backdrop="static">ΠΑΡΑΓΓΕΛΙΑ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</footer>
