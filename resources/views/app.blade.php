<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="{{ asset('img/default.png') }}">

        <title>{{ config('app.name') }}</title>

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @yield('head_stylesheets')

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        @yield('head_javascript')
    </head>
    <body>
        @include('navigation._top')

        <div class="container">
            @include('widgets._map')
            @include('widgets._modal_chart')
            @include('widgets._modal_list')
            @include('widgets._modal_login')
            @include('widgets._modal_order')
            @include('widgets._modal_orders')
            @include('widgets._modal_pinpoint')
            @include('widgets._modal_price')
            @include('widgets._modal_pricelist')
            @include('widgets._modal_register')
        </div>

        @include('navigation._footer')

        <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="https://maps.google.com/maps/api/js?libraries=geometry&amp;v=3.25&amp;key={{ env("GOOGLE_MAPS_API_KEY") }}"></script>
        <script src="https://www.gstatic.com/charts/loader.js" type="text/javascript"></script>
        <script src="{{ asset('js/maplace.min.js') }}"></script>
        <script src="{{ asset('js/js.cookie.js') }}"></script>
        <script src="{{ asset('js/validator.min.js') }}"></script>
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
        <script src="{{ asset('js/jquery.matchHeight-min.js') }}"></script>
        <script src="{{ asset('js/helpers.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('body_javascript')
    </body>
</html>
