<?php

use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {

    /** ID Pattern */
    Route::pattern('id', '[0-9]+');

    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('register', 'App\Api\V1\Controllers\AuthController@register');
        $api->post('login', 'App\Api\V1\Controllers\AuthController@login');
        $api->post('logout', 'App\Api\V1\Controllers\AuthController@logout');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->group(['prefix' => 'user'], function(Router $api) {
            $api->get('orders', 'App\Api\V1\Controllers\GasStationController@getOrders');
            $api->get('pricedata', 'App\Api\V1\Controllers\GasStationController@getPriceData');
            $api->put('pricedata', 'App\Api\V1\Controllers\GasStationController@putPriceData');
            $api->post('order', 'App\Api\V1\Controllers\GasStationController@postOrder');
        });

        $api->get('token/refresh', ['middleware' => 'jwt.refresh', function() {
			return response()->json(['status' => 'OK']);
		}]);
    });

    $api->group(['prefix' => 'fuel'], function(Router $api) {
        $api->get('count', 'App\Api\V1\Controllers\FuelController@getCountOfGasStations');
        $api->get('pricedata', 'App\Api\V1\Controllers\FuelController@getPriceDataPerLitre');
        $api->get('chartdata', 'App\Api\V1\Controllers\FuelController@getChartPriceData');
        $api->get('gasstations/{id}/pricedata', 'App\Api\V1\Controllers\FuelController@getPriceDataByGasStation');
        $api->get('gasstations', 'App\Api\V1\Controllers\FuelController@getAllGasStations');
        $api->get('gasstations/xml', 'App\Api\V1\Controllers\FuelController@getAllGasStationsAsXml');
    });
});
