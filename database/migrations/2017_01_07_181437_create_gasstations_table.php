<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGasStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gasstations', function (Blueprint $table) {
            $table->smallIncrements('gasStationID');
            $table->decimal('gasStationLat', 10, 7)->nullable();
            $table->decimal('gasStationLong', 10, 7)->nullable();
            $table->tinyInteger('fuelCompID');
            $table->string('fuelCompNormalName', 45);
            $table->string('gasStationOwner', 128);
            $table->string('ddID', 10);
            $table->string('ddNormalName', 45);
            $table->string('municipalityID', 10);
            $table->string('municipalityNormalName', 45);
            $table->string('countyID', 10);
            $table->string('countyName', 64);
            $table->string('gasStationAddress')->nullable();
            $table->char('phone1', 10)->nullable();
            $table->string('username', 45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gasstations');
    }
}
