<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriceDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricedata', function (Blueprint $table) {
            $table->unsignedSmallInteger('gasStationID');
            $table->unsignedTinyInteger('fuelTypeID');
            $table->unsignedTinyInteger('fuelSubTypeID');
            $table->string('fuelNormalName', 64);
            $table->string('fuelName', 128);
            $table->decimal('fuelPrice', 4, 3);
            $table->timestamp('dateUpdated')->nullable();
            $table->boolean('isPremium')->nullable();

            $table->foreign('gasStationID')->references('gasStationID')->on('gasstations')->onDelete('cascade');
            $table->primary(['gasStationID', 'fuelTypeID', 'fuelSubTypeID']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricedata');
    }
}
