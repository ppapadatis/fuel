<?php
return [
    'register' => [
        'release_token' => env('REGISTER_RELEASE_TOKEN'),
        'validation_rules' => [
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'is_owner' => 'boolean'
        ]
    ],
    'login' => [
        'validation_rules' => [
            'username' => 'required',
            'password' => 'required'
        ]
    ],
    'gas_station' => [
        'put_price_data' => [
            'validation_rules' => [
                'fuelTypeID' => 'required|integer|min:0',
                'fuelSubTypeID' => 'required|integer|min:0',
                'fuelName' => 'required|string|between:0,128',
                'fuelPrice' => 'required|numeric|min:0.000|max:99999.999',
                'isPremium' => 'boolean'
            ]
        ],
        'post_order' => [
            'validation_rules' => [
                'gasStationID' => 'required|integer|min:0|exists:gasstations,gasStationID',
                'quantity' => 'required|numeric|min:0.000|max:99999.999'
            ]
        ]
    ]
];
