(function (fuelHelpers, $, undefined) {
    'use strict';

    fuelHelpers.ajaxHelper = {
        postRegister: function (username, email, password, password_confirmation, is_owner) {
            return $.post('/api/auth/register', {
                username: username, email: email, password: password,
                password_confirmation: password_confirmation, is_owner: is_owner
            });
        },
        postLogin: function (username, password) {
            return $.post('/api/auth/login', { username: username, password: password });
        },
        postLogout: function () {
            return ajaxRequest('/api/auth/logout', 'POST');
        },
        getOrders: function () {
            return ajaxRequest('/api/user/orders', 'GET');
        },
        getPriceData: function () {
            return ajaxRequest('/api/user/pricedata', 'GET');
        },
        putPriceData: function (fuelTypeID, fuelSubTypeID, fuelName, fuelPrice, isPremium) {
            return ajaxRequest('/api/user/pricedata', 'PUT', {
                fuelTypeID: fuelTypeID, fuelSubTypeID: fuelSubTypeID,
                fuelName: fuelName, fuelPrice: fuelPrice, isPremium: isPremium
            });
        },
        postOrder: function (gasStationID, quantity) {
            return ajaxRequest('/api/user/order', 'POST', { gasStationID: gasStationID, quantity: quantity });
        },
        getRefreshToken: function () {
            return ajaxRequest('/api/token/refresh', 'GET');
        },
        getCount: function () {
            return ajaxRequest('/api/fuel/count', 'GET');
        },
        getPriceDataInfo: function () {
            return ajaxRequest('/api/fuel/pricedata', 'GET');
        },
        getChartPriceData: function () {
            return ajaxRequest('/api/fuel/chartdata', 'GET');
        },
        getGasStationPriceData: function (id) {
            return ajaxRequest('/api/fuel/gasstations/' + id + '/pricedata', 'GET');
        },
        getGasStations: function () {
            return ajaxRequest('/api/fuel/gasstations', 'GET');
        }
    };

    fuelHelpers.cookieHelper = {
        set: function(cname, cvalue) {
            Cookies.set(cname, cvalue, { expires: 1/24 });
        },
        get: function(cname) {
            return Cookies.get(cname);
        },
        check: function (cname) {
            var cvalue = this.get(cname);
            return typeof cvalue !== 'undefined';
        },
        remove: function (cname) {
            Cookies.remove(cname);
        }
    };

    fuelHelpers.mapHelper = {
        init: function () {
            window.maplace = new Maplace({
                map_options: {
                    set_center: [38.24, 24.15],
                    zoom: 7
                }
            }).Load();
        }
    };

    function ajaxRequest(url, type, data) {
        return $.ajax({
            url: url,
            type: type,
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer: ' + fuelHelpers.cookieHelper.get('fuelGR_token'))
            },
            data: data
        });
    }

}(window.fuelHelpers = window.fuelHelpers || {}, jQuery));
