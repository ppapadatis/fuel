(function () {
    // Initialize variables.
    var $loginModal = $('#loginModal'),
        $registerModal = $('#registerModal'),
        $listModal = $('#listModal'),
        $ordersModal = $('#ordersModal'),
        $priceListModal = $('#priceListModal'),
        $priceFormModal = $('#priceModal'),
        $orderModal = $('#orderModal'),
        $pinPointModal = $('#pinPointModal'),
        $chartModal = $('#chartModal');

    // Load google charts library.
    google.charts.load('visualization', '1', {packages: ['corechart']});

    // Prevent default functionality for hyperlink placeholders.
    $('a[href="#"]').click(function (evt) {
        evt.preventDefault();
    });

    // Update footer information.
    var gasStations = fuelHelpers.ajaxHelper.getCount(),
        prices = fuelHelpers.ajaxHelper.getPriceDataInfo();

    gasStations.done(function (data) {
        $('#total_stations').find('i').after(data);
    }).fail(function (xhr, status, response) {
        sweetAlert('ΣΦΑΛΜΑ', response, 'error');
    });

    prices.done(function (data) {
        $('#min_price').find('i').after(data.min + '€');
        $('#average_price').find('i').after(data.average + '€');
        $('#max_price').find('i').after(data.max + '€');
    }).fail(function (xhr, status, response) {
        sweetAlert('ΣΦΑΛΜΑ', response, 'error');
    });

    // Change site links for authenticated users.
    if (fuelHelpers.cookieHelper.check('fuelGR_token')) {
        $('.navbar-right li').not('.auth').addClass('hidden');
        $('.navbar-right li.public').removeClass('hidden');

        var userType = fuelHelpers.cookieHelper.get('fuelGR_user_type');
        // Gas station owner
        if (parseInt(userType) > 0) {
            $('.navbar-right li.owner').removeClass('hidden');
            $('.navbar-right li.user').addClass('hidden');
        } else {
            $('.navbar-right li.user').removeClass('hidden');
            $('.navbar-right li.owner').addClass('hidden');
        }
    } else {
        $('.navbar-right li.auth').addClass('hidden');
    }

    // Initialize map and add markers.
    fuelHelpers.mapHelper.init();
    if (maplace.Loaded()) {
        var markers = fuelHelpers.ajaxHelper.getGasStations();
        markers.done(function (data) {
            var locationsArray = [];

            $.each(data, function (index, location) {
                locationsArray.push({
                    lat: location.gasStationLat,
                    lon: location.gasStationLong,
                    title: location.fuelCompNormalName,
                    html: '<div class="caption">' +
                            '<img src="/img/' + location.fuelCompNormalName + '.png" ' +
                                'class="img-responsive center-block" ' +
                                'alt="' + location.fuelCompNormalName + '" />' +
                            '<h3>' + location.fuelCompNormalName + '</h3>' +
                            '<p>' + location.gasStationOwner + '</p>' +
                            '<p>' + location.gasStationAddress + '</p>' +
                            '<p>' + (location.phone1 ? location.phone1 : '') + '</p>' +
                            '<p><a href="#" class="btn btn-default" data-toggle="modal" data-target="#pinPointModal" data-id="' + location.gasStationID + '" data-backdrop="static">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</a></p>' +
                        '</div>',
                    icon: '/img/' + location.fuelCompNormalName + '.png'
                });

                // Add gas station information in a listing modal.
                $listModal.find('.modal-body .row').append(
                    '<div class="col-md-3 col-sm-6">' +
                        '<div class="thumbnail">' +
                            '<div class="caption">' +
                                '<img src="/img/' + location.fuelCompNormalName + '.png" ' +
                                    'class="img-responsive center-block" ' +
                                    'alt="' + location.fuelCompNormalName + '" />' +
                                '<h3>' + location.fuelCompNormalName + '</h3>' +
                                '<p>' + location.gasStationOwner + '</p>' +
                                '<p>' + location.gasStationAddress + '</p>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                );
            });

            $listModal.find('.thumbnail').matchHeight();
            maplace.AddLocations(locationsArray, true);
        }).fail(function (xhr, status, response) {
            sweetAlert('ΣΦΑΛΜΑ', response, 'error');
        });
    }

    $loginModal.on('hide.bs.modal', function () {
        resetModalFormElements($(this));
    });
    $registerModal.on('hide.bs.modal', function () {
        resetModalFormElements($(this));
    });

    $ordersModal.on('show.bs.modal', function () {
        var $modal = $(this);

        var request = fuelHelpers.ajaxHelper.getOrders();
        request.done(function (data) {
            if (data && data.length > 0) {
                var body = '';
                $.each(data, function (index, order) {
                    body += '<tr><td>' + order.username + '</td>' +
                        '<td>' + order.quantity + '</td><td>' + order.created_at + '</td></tr>';
                });
                $modal.find('.table-responsive').removeClass('hidden').find('tbody').html(body);
            } else {
                $modal.find('.alert').removeClass('hidden');
            }
        }).fail(function (xhr, status, response) {
            refreshToken();
        });
    }).on('hide.bs.modal', function () {
        resetRequestModal($(this))
    });

    $priceListModal.on('show.bs.modal', function () {
        var $modal = $(this);

        var request = fuelHelpers.ajaxHelper.getPriceData();
        request.done(function (data) {
            if (data && data.length > 0) {
                var body = '';
                $.each(data, function (index, product) {
                    body += '<tr><td>' + product.fuelName + '</td>' +
                        '<td>' + product.fuelPrice + '</td>' +
                        '<td>' + (product.isPremium ? 'ΝΑΙ' : 'ΟΧΙ') + '</td>' +
                        '<td>' + product.dateUpdated + '</td>' +
                        '<td><a href="#" class="btn btn-warning btn-xs" title="ΕΠΕΞΕΡΓΑΣΙΑ" ' +
                            'data-toggle="modal" data-target="#priceModal" data-dismiss="modal" ' +
                            'data-fueltypeid="' + product.fuelTypeID + '" ' +
                            'data-fuelsubtypeid="' + product.fuelSubTypeID + '" ' +
                            'data-fuelname="' + product.fuelName + '" ' +
                            'data-fuelprice="' + product.fuelPrice + '" ' +
                            'data-ispremium="' + product.isPremium + '" ' +
                            'data-backdrop="static">' +
                        '<i class="material-icons">mode_edit</i></a></td></tr>';
                    $modal.find('.table-responsive').removeClass('hidden').find('tbody').html(body);
                });
            } else {
                $modal.find('.alert').removeClass('hidden');
            }
        }).fail(function (xhr, status, response) {
            refreshToken();
        })
    }).on('hide.bs.modal', function () {
        resetRequestModal($(this))
    });

    $pinPointModal.on('show.bs.modal', function (evt) {
        var caller = $(evt.relatedTarget),
            gasStationID = caller.data('id'),
            $modal = $(this);

        var request = fuelHelpers.ajaxHelper.getGasStationPriceData(gasStationID);
        request.done(function (data) {
            if (data && Object.keys(data).length > 0) {
                var body = '';
                $.each(data, function (index, product) {
                    body += '<tr><td>' + product.fuelName + '</td>' +
                        '<td>' + product.fuelPrice + '</td>' +
                        '<td>' + (product.isPremium ? 'ΝΑΙ' : 'ΟΧΙ') + '</td>' +
                        '<td>' + product.dateUpdated + '</td></tr>';
                    $modal.find('.table-responsive').removeClass('hidden').find('tbody').html(body);
                });
            } else {
                $modal.find('.alert').removeClass('hidden');
            }
        }).fail(function (xhr, status, response) {
            sweetAlert('ΣΦΑΛΜΑ', response, 'error');
        })
    }).on('hide.bs.modal', function () {
        resetRequestModal($(this))
    });

    $priceFormModal.on('show.bs.modal', function (evt) {
        var caller = $(evt.relatedTarget),
            fuelTypeID = caller.data('fueltypeid'),
            fuelSubTypeID = caller.data('fuelsubtypeid'),
            fuelName = caller.data('fuelname'),
            fuelPrice = caller.data('fuelprice'),
            isPremium = caller.data('ispremium'),
            $modal = $(this);

        $modal.find('#fuelTypeID').val(fuelTypeID);
        $modal.find('#fuelSubTypeID').val(fuelSubTypeID);
        $modal.find('#fuelName').val(fuelName);
        $modal.find('#fuelPrice').val(fuelPrice);
        if (parseInt(isPremium) > 0) {
            $modal.find('#isPremium').prop('checked', true);
        }
        $modal.find('form').validator('destroy').validator();
    }).on('hide.bs.modal', function () {
        resetModalFormElements($(this));
    });

    $orderModal.on('show.bs.modal', function () {
        var $modal = $(this);

        markers.done(function (data) {
            var locationsArray = ['<option value="" selected="selected">ΕΠΙΛΟΓΗ ΚΑΤΑΣΤΗΜΑΤΟΣ</option>'];
            $.each(data, function (index, location) {
                locationsArray.push('<option value="' + location.gasStationID + '">' + location.gasStationOwner + '</option>');
            });

            $modal.find('select').html(locationsArray.join(''));
        }).fail(function (xhr, error, response) {
            sweetAlert('ΣΦΑΛΜΑ', response, 'error');
        });
    }).on('hide.bs.modal', function () {
        resetModalFormElements($(this))
    });

    $chartModal.on('show.bs.modal', function () {
        var request = fuelHelpers.ajaxHelper.getChartPriceData();

        request.done(function (data) {
            google.charts.setOnLoadCallback(drawBasic(data));
        }).fail(function (xhr, error, response) {
            sweetAlert('ΣΦΑΛΜΑ', response, 'error');
        });
    }).on('hide.bs.modal', function () {
        $('.modal-body', $(this)).empty().append('<div id="chart_div"></div>');
    });

    function drawBasic(data) {
        var chartData = google.visualization.arrayToDataTable(data);

        var options = {
            title: 'ΠΟΙΚΙΛΙΑ ΚΑΥΣΙΜΩΝ',
            hAxis: {title: 'ΠΡΑΤΗΡΙΑ'}
        };

        var chart = new google.visualization.ColumnChart($('#chart_div')[0]);
        chart.draw(chartData, options);
    }

    // Resize charts on window resize event.
    $(window).resize(function () {
        drawBasic();
    });

    // Add listeners for (in)valid form inputs.
    $(document).on('invalid.bs.validator', 'form', function () {
        $(this).find(':submit').prop('disabled', true);
    }).on('valid.bs.validator', 'form', function () {
        var $submit = $(this).find(':submit'),
            errorFlag = false;

        if ($('.has-errors', $(this)).length > 0) {
            errorFlag = true;
        }

        $(':input[required]', $(this)).each(function () {
            if ($(this).val().trim() === '') {
                errorFlag = true
            }
        });

        $submit.prop('disabled', errorFlag);
    });

    // Perform login.
    $(document).on('click', '.login', function (evt) {
        evt.preventDefault();

        var $button = $(evt.target),
            request = fuelHelpers.ajaxHelper.postLogin($('#login_username').val(), $('#login_password').val());

        $button.prop('disabled', true);

        request.done(function (data) {
            fuelHelpers.cookieHelper.set('fuelGR_token', data.token);
            fuelHelpers.cookieHelper.set('fuelGR_user_type', data.user_type);
            location.reload();
        }).fail(function (xhr, status, response) {
            $button.prop('disabled', false);
            var errors = '';
            $.each(xhr.responseJSON.errors, function (index, error) {
                errors += '<p>' + error + '</p>';
            });
            sweetAlert({
                title: 'ΣΦΑΛΜΑ',
                text: errors,
                type: 'error',
                html: true
            });
        });
    });

    // Perform logout.
    $(document).on('click', '.logout', function (evt) {
        evt.preventDefault();

        var request = fuelHelpers.ajaxHelper.postLogout();

        request.always(function () {
            fuelHelpers.cookieHelper.remove('fuelGR_token');
            fuelHelpers.cookieHelper.remove('fuelGR_user_type');
            location.reload();
        });
    });

    // Perform registration.
    $(document).on('click', '.register', function (evt) {
        evt.preventDefault();

        var $button = $(evt.target),
            request = fuelHelpers.ajaxHelper.postRegister(
                $('#username').val(),
                $('#email').val(),
                $('#password').val(),
                $('#password_confirmation').val(),
                $('#is_owner').is(':checked') ? 1 : 0
            );

        $button.prop('disabled', true);

        request.done(function (data) {
            fuelHelpers.cookieHelper.set('fuelGR_token', data.token);
            fuelHelpers.cookieHelper.set('fuelGR_user_type', data.user_type);
            location.reload();
        }).fail(function (xhr, status, response) {
            $button.prop('disabled', false);
            var errors = '';
            $.each(xhr.responseJSON.errors, function (index, error) {
                errors += '<p>' + error + '</p>';
            });
            sweetAlert({
                title: 'ΣΦΑΛΜΑ',
                text: errors,
                type: 'error',
                html: true
            });
        });
    });

    // Perform price update.
    $(document).on('click', '.price_update', function (evt) {
        evt.preventDefault();

        var $button = $(evt.target),
            request = fuelHelpers.ajaxHelper.putPriceData(
                $('#fuelTypeID').val(),
                $('#fuelSubTypeID').val(),
                $('#fuelName').val(),
                $('#fuelPrice').val(),
                $('#isPremium').is(':checked') ? 1 : 0
            );

        $button.prop('disabled', true);

        request.done(function (data) {
            $button.prev().click();
        }).fail(function (xhr, status, response) {
            if (response === 'Unauthorized') {
                refreshToken();
            } else {
                $button.prop('disabled', false);
                var errors = '';
                $.each(xhr.responseJSON.errors, function (index, error) {
                    errors += '<p>' + error + '</p>';
                });
                sweetAlert({
                    title: 'ΣΦΑΛΜΑ',
                    text: errors,
                    type: 'error',
                    html: true
                });
            }
        });
    });

    // Perform order.
    $(document).on('click', '.order', function (evt) {
        evt.preventDefault();

        var $button = $(evt.target),
            request = fuelHelpers.ajaxHelper.postOrder(
                $('#gasStationID').val(),
                $('#quantity').val()
            );

        $button.prop('disabled', true);

        request.done(function () {
            $orderModal.modal('hide');
        }).fail(function (xhr, status, response) {
            if (response === 'Unauthorized') {
                refreshToken();
            } else {
                $button.prop('disabled', false);
                var errors = '';
                $.each(xhr.responseJSON.errors, function (index, error) {
                    errors += '<p>' + error + '</p>';
                });
                sweetAlert({
                    title: 'ΣΦΑΛΜΑ',
                    text: errors,
                    type: 'error',
                    html: true
                });
            }
        });
    });

    /**
     * Refreshes the user token.
     */
    function refreshToken() {
        var request = fuelHelpers.ajaxHelper.getRefreshToken();
        request.fail(function (xhr, error, response) {
            fuelHelpers.cookieHelper.remove('fuelGR_token');
            fuelHelpers.cookieHelper.remove('fuelGR_user_type');
            location.reload();
        });
    }

    /**
     * Resets all form elements inside a modal's form.
     *
     * @param {object} modal
     */
    function resetModalFormElements(modal) {
        modal.find('form').find('input, select').val('');
        modal.find('form').validator('destroy').validator();
    }

    /**
     * Resets visible elements inside a modal's form.
     *
     * @param {object} modal
     */
    function resetRequestModal(modal) {
        if (!modal.find('.table-responsive').hasClass('hidden')) {
            modal.find('.table-responsive').addClass('hidden');
        }

        if (!modal.find('.alert').hasClass('hidden')) {
            modal.find('.alert').addClass('hidden')
        }
    }
})();
